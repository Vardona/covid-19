document.getElementById("comment-box").addEventListener("click", function () {
    document.querySelector(".popup").style.display = "flex";
})
document.getElementById("close").addEventListener("click", function () {
    document.querySelector(".popup").style.display = "none";
})


var id = 0
var cards = []
function deleteCard(id) {
    var delCard = document.getElementById(id)
    document.getElementById('cardContainer').removeChild(delCard)
    delete cards[id]
    id--

}

function showComments() {
    document.getElementById('cardContainer').innerHTML = ''

    cards.forEach(function (element) {
        var cardContainer = document.getElementById('cardContainer')
        var cardDiv = document.createElement('div')
        cardDiv.classList.add('col-lg-3')
        cardDiv.className = "commentbox"
        cardDiv.classList.add('card')
        cardDiv.style.backgroundColor = '#34495E'
        cardDiv.setAttribute('id', element.id)
        var headDiv = document.createElement('div')
        headDiv.classList.add('text-right')

        var delIcon = document.createElement('i')
        delIcon.classList.add('fa')
        delIcon.classList.add('fa-times')
        delIcon.classList.add('delCard')
        delIcon.addEventListener("click", function () { deleteCard(element.id) })

        var title = document.createElement('h3')
        title.className = "Name"
        title.innerHTML = element.title
        title.style.borderBottom = "3px solid #ffc311";
        // title.style.display = "inline"
        var desc = document.createElement('p')
        desc.className = "text"
        desc.innerHTML = element.description

        var date = document.createElement('p')
        date.innerHTML = element.date
        date.style.fontWeight = "bold";

        var user = document.createElement('i');
        user.classList.add('fas');
        user.classList.add('fa-user');
        user.style.marginRight = "10px"

        var inlineShow = document.createElement('div')
        inlineShow.appendChild(user)

        inlineShow.appendChild(title)
        // user.style.display = "inline"

        headDiv.appendChild(delIcon)
        cardDiv.appendChild(headDiv)
        // cardDiv.appendChild(user);
        // cardDiv.appendChild(title)
        cardDiv.appendChild(inlineShow)
        cardDiv.appendChild(desc)
        cardDiv.appendChild(date)
        cardDiv.style.display = "flex"
        cardDiv.style.flexDirection = "column"
        cardContainer.appendChild(cardDiv)

    })
}
function addNewCard() {
    let d = new Date()
    let month = d.getMonth() + 1
    date = d.getDate() + '/' + month + '/' + d.getFullYear()
    var title = document.getElementById('title')
    var description = document.getElementById('description')
    let card = { id: id, title: title.value, description: description.value, date: date }
    cards.push(card)
    id++
    showComments()
    document.querySelector(".popup").style.display = "none";


}